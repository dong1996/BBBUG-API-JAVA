package com.bbbug.coree.entity;

import com.alibaba.fastjson.JSONObject;
import com.bbbug.coree.utils.MiscUtil;
import lombok.Data;

import java.io.Serializable;
@Data

public class Messageinfo implements Serializable {
    public Messageinfo(Integer message_user, String message_type, String message_where, Integer message_to, JSONObject message_content, Integer message_status) {
        this.message_id = message_content.getString("message_id");
        this.message_user = message_user;
        this.message_type = message_type;
        this.message_where = message_where;
        this.message_to = message_to;
        this.message_content = message_content.toJSONString();
        this.message_status = message_status;
        this.message_createtime = System.currentTimeMillis();
        this.message_updatetime = System.currentTimeMillis();
    }

    public Messageinfo() {
    }

    private static final long serialVersionUID = 240926990378090534L;
    //消息id
    private String  message_id;
    //消息发送者
    private Integer  message_user;
    //消息类型
    private String  message_type;
    //消息发送地
    private String  message_where;
    //消息接受地
    private Integer  message_to;
    //消息正文
    private String  message_content;
    //消息状态
    private Integer  message_status;
    //消息创建时间
    private Long  message_createtime;
    //消息更新时间
    private Long  message_updatetime;
}
