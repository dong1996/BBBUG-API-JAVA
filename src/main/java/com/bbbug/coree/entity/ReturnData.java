package com.bbbug.coree.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;
@Data
public class ReturnData implements Serializable {
    private static final long serialVersionUID = -65536L;
    //消息返回值
    Integer code;
    //返回信息
    String msg;
    //返回数据
    Object data;
    public ReturnData(Integer code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
}
