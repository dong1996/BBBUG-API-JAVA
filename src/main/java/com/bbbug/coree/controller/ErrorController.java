package com.bbbug.coree.controller;

import com.bbbug.coree.entity.ReturnData;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@CrossOrigin(origins ="${myweb-path}" )
@RequestMapping("/error")
public class ErrorController {
    @RequestMapping("/404")
    ReturnData serverNotfound(HttpServletRequest httpServletRequest){
        return new ReturnData(404,"无此页面",httpServletRequest.getRequestURI());
    }
    @RequestMapping("/500")
    ReturnData serverError(HttpServletRequest httpServletRequest){
        return new ReturnData(500,"出问题辣",httpServletRequest.getRequestURI());
    }
    @RequestMapping("/400")
    ReturnData serverBadRequest(HttpServletRequest httpServletRequest){
        return new ReturnData(400,"坏请求",httpServletRequest.getRequestURI());
    }
}
